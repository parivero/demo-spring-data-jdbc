package com.demo.data.jdbc.repository;

import com.demo.data.jdbc.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=ApplicationConfig.class)
public class PersonaRepositoryTest {
    
    @Autowired
    private PersonaRepository personaRepository;

    @Test
    public void findAll_retornaTodos() {
    }
    
    @Test
    public void save_retornaPersona() {
    }
    
    @Test
    public void findByNombreLike_retornaPersonas() {
    }
}
