package com.demo.data.jdbc.domain;

import lombok.Data;

@Data
public class Persona {

    private Long id;
    private String nombre;

}
