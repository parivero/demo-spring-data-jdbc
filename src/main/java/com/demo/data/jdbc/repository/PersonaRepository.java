package com.demo.data.jdbc.repository;

import com.demo.data.jdbc.domain.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long> {
}
