create table persona(
    id bigint identity primary key not null,
    nombre varchar(200) 
);

INSERT INTO persona(nombre)
VALUES('Josefina'),
      ('Tomi'),
      ('Felipe'),
      ('Nacho');